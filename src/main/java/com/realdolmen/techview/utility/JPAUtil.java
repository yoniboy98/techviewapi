/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.com.realdolmen.techview.utility;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author YVDBL89
 * Create jpa utility */


public class JPAUtil {
    private final EntityManagerFactory emf;
    private static JPAUtil jpaUtility;


    public void close() {
        emf.close();
    }

    public EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public JPAUtil() {
        emf = Persistence.createEntityManagerFactory("com.yoni_techviewApi_war_1.0-SNAPSHOTPU");
    }

    public static JPAUtil getInstance() {
        if (jpaUtility == null) {
            jpaUtility = new JPAUtil();
        }
        return jpaUtility;
    }
}

