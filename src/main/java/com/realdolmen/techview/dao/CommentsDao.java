/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.com.realdolmen.techview.dao;

import java.util.List;
import javax.persistence.*;

import org.springframework.stereotype.Service;
import src.com.realdolmen.techview.beans.Comments;
import src.com.realdolmen.techview.utility.JPAUtil;


/**
 * @author YVDBL89
 */
@Service
public class CommentsDao extends MainDao {


    public CommentsDao() {
        super();
    }

    protected CommentsDao(JPAUtil jpaUtil) {
        super(jpaUtil);
    }

    /**
     * Search comments on id in my DB
     */
    public Comments findById(long id) {
        em = jpaUtil.createEntityManager();
        TypedQuery<Comments> query = em.createNamedQuery("Comments.findById", Comments.class);
        query.setParameter("id", id);
        Comments comment = query.getSingleResult();
        em.close();
        return comment;
    }

    /**
     * Search on productId so I can specify the right comments to the right product.
     */
    public List<Comments> findByProductId(String productId) {
        em = jpaUtil.createEntityManager();
        TypedQuery<Comments> query = em.createNamedQuery("Comments.findByProductId", Comments.class);
        query.setParameter("productId", productId);
        List<Comments> commentId;
        try{
            commentId = query.getResultList();
        } catch(NoResultException e){
            commentId = null;
        }
        em.close();
        return commentId;
    }

    /**
     * Get all comments from the DB.
     */
    public List<Comments> findAll() {
        em = jpaUtil.createEntityManager();
        TypedQuery<Comments> query = em.createNamedQuery("Comments.findAll", Comments.class);

        List<Comments> comments = query.getResultList();
        em.close();
        return comments;
    }




   /*

    public boolean add(Comments comment) {
        em = jpaUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.persist(comment);
            tx.commit();
            return true;
        } catch (RollbackException ex) {
            return false;
        } finally {
            em.close();
        }
    }

    public void delete(int id) {
        Comments comment = findById(id);
        em = jpaUtil.createEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.remove(comment);
            tx.commit();

        } catch (RollbackException e) {

        } finally {
            em.close();
        }
    }
*/
}
